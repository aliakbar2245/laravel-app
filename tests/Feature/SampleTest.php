<?php

namespace Tests\Feature;

use App\Data\Bar;
use App\Data\Foo;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsString;
use function PHPUnit\Framework\assertTrue;

class SampleTest extends TestCase
{
    public function testGetEnv()
    {
        $appUrl = env("APP_URL");
        assertEquals('http://localhost', $appUrl);
    }

    public function testDefaultEnv()
    {
        $default = env('APP_URL', 'localhost');
        assertEquals('http://localhost', $default);
    }

    public function testAppProfile()
    {
        $profile = App::environment(['testing', 'local', 'dev', 'prod']);
        if ($profile == 'testing') {
            assertTrue($profile);
        } elseif ($profile == 'local') {
            assertIsString('local', $profile);
        } elseif ($profile == 'dev') {
            assertIsString('dev', $profile);
        } elseif ($profile == 'prod') {
            assertIsString('prod', $profile);
        }
    }

    public function testDepedencyInjection()
    {
        $foo = new Foo();
        $bar = new Bar($foo);
        assertEquals('Foo and Bar', $bar->bar());
    }
}
