<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function hello(string $who): string
    {
        return "Hello World " . $who;
    }
}
