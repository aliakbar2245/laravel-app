<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::fallback(function () {
    return view('fallback');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get("/hello/{who}", function ($who) {
    return "Hello Amigos, " . $who . "]";
});

Route::get('/hi/{who}', [\App\Http\Controllers\HelloController::class, 'hello']);
