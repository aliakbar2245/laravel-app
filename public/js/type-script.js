var typed = new Typed('.typing-text', {
    strings: ['Java Developer', 'Backend Developer', 'Data Engineer', 'Fullstack Developer'],
    typeSpeed: 120,
    loop: true
});