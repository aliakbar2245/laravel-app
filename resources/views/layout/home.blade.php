<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Title -->
    <title>Ali Akbar | Personal Portfolio</title>

    <!-- Font-Awesome CSS -->
    <link href="{{ asset('vendors/font-awesome/css/all.min.css') }}" rel="stylesheet">

    <!-- Swiper CSS -->
    <link href="{{ asset('vendors/swiper/swiper.css') }}" rel="stylesheet">

    <!-- Magnific-Popup CSS -->
    <link href="{{ asset('vendors/magnific-popup/magnific-popup.css') }}" rel="stylesheet">

    <!-- Custom Stylesheets -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body>
    @include('layout.partial.navbar')

    <main role="main">
        @yield('content')
    </main>

    @include('layout.partial.footer')
</body>

</html>
