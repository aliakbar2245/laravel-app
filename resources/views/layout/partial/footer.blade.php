<!-------------------- Footer Area (Start) -------------------->
<footer>
    <div class="social">
        <a href="{{ secure_url('https://web.facebook.com/aliakbar53') }}" class="fab fa-facebook-f" target="_blank"></a>
        <a href="#" class="fab fa-twitter"></a>
        <a href="{{ secure_url('https://instagram.com/aliakbar53') }}" class="fab fa-instagram" target="_blank"></a>
        <a href="{{ secure_url('https://www.linkedin.com/in/ali-akbar-52033785') }}" class="fab fa-linkedin-in"
            target="_blank"></a>
    </div>

    <div class="content">
        <p>created by <span>aliakbar53</span> | all rights reserved</p>
    </div>
</footer>
<!-------------------- Footer Area (End) -------------------->



<!-- JQuery -->
<script src="{{ asset('vendors/jquery/jquery-3.6.0.js') }}" defer></script>

<!-- SWIPER JS -->
<script src="{{ asset('vendors/swiper/swiper.js') }}" defer></script>

<!-- Magnific-Popup JS -->
<script src="{{ asset('vendors/magnific-popup/jquery.magnific-popup.js') }}" defer></script>

<!-- Typed JS -->
<script src="{{ asset('vendors/typed/typed.js') }}" defer></script>


<!-- Custom Script File -->
<script src="{{ asset('js/script.js') }}" defer></script>
<script src="{{ asset('js/scroll-spy.js') }}" defer></script>
<script src="{{ asset('js/counter-up.js') }}" defer></script>
<script src="{{ asset('js/portfolio.js') }}" defer></script>
<script src="{{ asset('js/testi-slider.js') }}" defer></script>
<script src="{{ asset('js/blog-slider.js') }}" defer></script>
<script src="{{ asset('js/type-script.js') }}" defer></script>


<!-- Bootstrap core JavaScript-->
<script src="{{ asset('vendors/jquery/jquery.min.js') }}" defer></script>
<script src="{{ asset('vendors/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('vendors/jquery-easing/jquery.easing.min.js') }}" defer></script>

<!-- Custom scripts for all pages-->>
<script src="{{ asset('js/sb-admin-2.min.js') }}" defer></script>

<!---------------- Custom Script ---------------->

<!-- Contact-Form Script -->
@push('scripts')
    <script>
        jQuery('#contactUs-Form').on('submit', function(e) {
            jQuery('#msg').html('');
            jQuery('#submit').html('Please wait');
            jQuery('#submit').attr('disabled', true);
            jQuery.ajax({
                url: 'assets/php/submit.php',
                type: 'post',
                data: jQuery('#contactUs-Form').serialize(),
                success: function(result) {
                    jQuery('#msg').html(result);
                    jQuery('#submit').html('Submit');
                    jQuery('#submit').attr('disabled', false);
                    jQuery('#contactUs-Form')[0].reset();
                }
            });
            e.preventDefault();
        });
    </script>
@endpush
