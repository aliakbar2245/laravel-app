<!-------------------- Scroll-Top Area (Start) -------------------->
<a href="#home" class="scroll-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-------------------- Scroll-Top Area (End) -------------------->

<!-------------------- Header Area (Start) -------------------->
<header class="header">

    <!-- Logo -->
    <a class="logo" href="#home">
        <h2> Ali Akbar </h2>
        <!-- <img src="images/logo.png" alt="logo" /> -->
    </a>
    <!-- Logo End -->

    <!-- Navbar -->
    <nav class="navbar">
        <a href="#home">home</a>
        <a href="#about">about</a>
        <a href="#service">portfolio</a>
    </nav>

    <!-- Menubar -->
    <div class="fas fa-bars" id="menu-btn"></div>

</header>
<!-------------------- Header Area (End) -------------------->
