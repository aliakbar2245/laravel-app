@extends('layout.home')

@section('content')
    <!-------------------- Home Area (Start) -------------------->
    <section class="home" id="home">

        <div class="box-container">

            <div class="content">
                <div class="text">
                    <h2>hi there!</h1>
                        <h1>I Am a <span class="typing-text"></span></h1>
                        <p>With over 5+ years of diverse IT experience spanning various roles and industries, I've
                            journeyed through the realms of banking, multifinance, life insurance, and education. My career
                            has been a
                            thrilling adventure, navigating the ever-evolving landscape of information technology to deliver
                            innovative solutions and drive success across these dynamic sectors</p>
                        <a href="{{ secure_url('https://www.linkedin.com/in/ali-akbar-52033785') }}" class="btn"
                            target="_blank">About Me<i class="fas fa-user"></i></a>
                </div>
            </div>

            <div class="image">
                <img src="images/Home/me_in_home.png" alt="Home-Image">
            </div>

        </div>

    </section>

    <!-------------------- Home Area (End) -------------------->


    <!-------------------- About Area (Start) -------------------->
    <section class="about" id="about">

        <div class="box-container">

            <div class="image">
                <img src="images/About/me_in_about.png" alt="About-Image" />
            </div>

            <div class="content">

                <h2>My name is Ali Akbar</h2>
                <p>I'm a passionate IT developer with a relentless enthusiasm for crafting innovative solutions. With a
                    strong background in various IT roles, I thrive on the challenges and excitement that the IT world
                    offers. I'm committed to pushing boundaries, staying at the forefront of technology, and creating
                    impactful applications that make a difference</p>

                <div class="about-info">

                    <div class="info">
                        <h4>age: <span>28</span></h4>
                        <h4>gender: <span>male</span></h4>
                        <h4>languages: <span>bahasa, english</span></h4>
                        <h4>Experiences: <span>Data Engineer, Fullstack Developer and Backend Developer</span></h4>
                        <h4>status: <span>open to work</span></h4>
                        <a href="{{ secure_url('https://drive.google.com/file/d/1AXSAb8jE2-4Dtt78-bYAqMGe5dDmeJ9E/view?usp=sharing') }}"
                            class="btn" target="_blank">download CV <i class="fas fa-download"></i></a>
                    </div>

                    <div class="counting">

                        <div class="box">
                            <h1 class="count" data-count="4">4+</h1>
                            <h3>years of <br />experience</h3>
                        </div>

                        <div class="box">
                            <h1 class="count" data-count="20">20+</h1>
                            <h3>projects <br />completed</h3>
                        </div>

                        <div class="box">
                            <h1 class="count" data-count="5">5+</h1>
                            <h3>great<br />empowering</h3>
                        </div>

                        <div class="box">
                            <h1 class="count" data-count="100">100+</h1>
                            <h3>great<br />behaviors</h3>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="skill-container">

            <div class="heading">
                <h2>my <span>skills</span></h2>
            </div>

            <div class="skills">
                <div class="skill-item item2">
                    <div class="box">
                        <h3>Languages</h3>
                    </div>
                    <div class="box">
                        <h3>PHP<span> 85% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Java<span> 90% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Python<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>CSS<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Javascript<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                    </div>
                </div>

                <div class="skill-item item1">
                    <div class="box">
                        <h3>Relational Databases</h3>
                    </div>
                    <div class="box">
                        <h3>PostgreSQL<span> 95% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Oracle<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>MySQL<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>SQL Server<span> 65% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>DB2<span> 60% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                    </div>
                </div>

                <div class="skill-item item3">
                    <div class="box">
                        <h3>Web Servers & Containerization</h3>
                    </div>
                    <div class="box">
                        <h3>AWS Cloud<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Apache Tomcat<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>JBoss<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Web Sphere<span> 50% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Docker<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                    </div>
                </div>

                <div class="skill-item item4">
                    <div class="box">
                        <h3>Security & Testing</h3>
                    </div>
                    <div class="box">
                        <h3>Nexus<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Fortify<span> 72% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>AquaSec<span> 60% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Unit Test<span> 62% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                    </div>
                </div>

                <div class="skill-item item5">
                    <div class="box">
                        <h3>Frameworks</h3>
                    </div>
                    <div class="box">
                        <h3>Spring-Boot<span> 87% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Laravel<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Struts<span> 75% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                </div>

                <div class="skill-item item6">
                    <div class="box">
                        <h3>Version Control Systems</h3>
                    </div>
                    <div class="box">
                        <h3>GitHub<span> 75% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>GitLab<span> 86% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Bitbucket<span> 85% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                </div>

                <div class="skill-item item7">
                    <div class="box">
                        <h3>CI/CD</h3>
                    </div>
                    <div class="box">
                        <h3>Jenkins<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Gitlab<span> 70% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                </div>

                <div class="skill-item item8">
                    <div class="box">
                        <h3>Others</h3>
                    </div>
                    <div class="box">
                        <h3>MongoDB/NoSQL<span> 68% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>Elasticsearch<span> 65% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>PL/SQL<span> 80% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                    <div class="box">
                        <h3>UNIX or Shellscript<span> 82% </span> </h3>
                        <div class="progress-bar"> <span></span> </div>
                    </div>
                </div>

            </div>

        </div>

    </section>
    <!-------------------- About Area (End) -------------------->



    <!-------------------- Qualification Area (Start) -------------------->
    <section class="qualification">

        <div class="heading">
            <h2>my <span>qualifications</span></h2>
        </div>

        <div class="box-container">

            <div class="education">
                <div class="experience-item">
                    <i class="fas fa-graduation-cap"></i>
                    <div class="content">
                        <span>2013 - 2017</span>
                        <h3>Computer Science, Perbanas Institute</h3>
                        <p><strong>GPA 3.56/4.00, Karet Kuningan - South Jakarta</strong>
                            <br/><br/>
                            Perbanas Institute is one of the first educational institutions that provides education
                            in the fields of Finance-Banking and Informatics, established by the Perbanas Education 
                            Foundation (National Bank Association) in 1969. Perbanas Institute has 2 campuses, 
                            located in Jakarta and Bekasi.
                            <br /><br />
                            More information : https://perbanas.id
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-graduation-cap"></i>
                    <div class="content">
                        <span>2010 - 2013</span>
                        <h3>MIPA, 53 Jakarta High School</h3>
                        <p>Average 75.00/100, Cipinang - East Jakarta
                            <br/><br/>
                            At that time, SMA Negeri 53 Jakarta was a distant branch (filial) of SMA 14 Cililitan 
                            in East Jakarta. Later, according to Decree No. 0220/0/1981 issued by the Ministry of 
                            Education and Culture, it was officially established as SMA Negeri 53 Jakarta, and the 
                            inauguration took place on March 27, 1982. In mid-2005, under the leadership of 
                            Drs. H. Sudirman Bur MM, the SMA building underwent a renovation. The new building was 
                            effectively put into use in January 2006. During the renovation period from mid-2005 to 
                            early 2006, the students and staff of SMA Negeri 53 conducted their teaching and learning 
                            activities in the building of SDN Cipinang Besar Selatan in the afternoon session
                        </p>
                    </div>
                </div>

            </div>

            <div class="experience">
                
                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>11.2023 - Now</span>
                        <h3>Backend Developer (Infosyst Solusi Terpadu)</h3>
                        <p>PT. Infosys Solusi Terpadu (IST) are a solution integrator company that focus primarily to provide 
                            innovative digital solutions in the Financial Services Industry, public sector, 
                            telecommunication industry and other industry. IST also provide digital products and solution.
                            I as part of backend team, helping client's service (BANK BTN) such API, bugs-fixing and new feature.
                            Currently we are user java-springboot, postgreSQL, GCP (Google Cloud Platform), Docker, Jenkins and etc
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>05.2022 - 11.2023</span>
                        <h3>Backend Developer (G2Academy)</h3>
                        <p>G2Academy is Education Platform to encourage people to be aware of
                            technology advances and needs in various industrial sector in global. As
                            part of product team, i as backend developer helping the stackholders
                            to create new feature or maintenance platform services CMS, CRM,
                            Admin Platform, Instructor Platform, and marketplace. The technology
                            used especially in backend is java spring-hibernate as framework,
                            PosgreSQL as Database, Gitlab as code repository management, Gitlab
                            CI/CD as integration deployment, Python as job scheduler, and AWS as
                            Cloud Server
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>01.2021 - 05.2022</span>
                        <h3>Backend Developer (Prudential Life Assurance)</h3>
                        <p>As part of the claims team, I help with stabilization for digital, minor,
                            and major products. help users to automate using microservices. The
                            technology used especially in the backend is java spring-hibernate,
                            posgre, DB2, SQL Server, Nexus, Fortify, quarkus, deployment
                            automation with jenkins, and using a linux-based server
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>10.2019 - 01.2021</span>
                        <h3>Senior Officer Programmer (AEON Credit)</h3>
                        <p>Part of the System especially in Development, I help for maintenance
                            debug an fix the issue whether in data, sql, config, and code inside the
                            services. Also we collaborate with vendor for enhance and resource. The
                            technology is java native, jsp, laravel, Rest API. Database is Oracle,
                            MySQL, and DB2. Also have good operations of linux
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>09.2018 - 10.2019</span>
                        <h3>Programmer (NEXT TI)</h3>
                        <p>As a part of Hana Group, Next TI is the new company (startup) working
                            in IT Solution Core Banking Sytem. I help for maintenance debug an fix
                            the issue whether in data, sql, and code inside the services. Also
                            maintenance batch job for ETL process with shellscript in MIS regarding
                            OJK and BI report. The technology is java struts framework, Adobe Flex,
                            Oracle, have good operations of linux, and have good knowlaged bash
                        </p>
                    </div>
                </div>

                <div class="experience-item">
                    <i class="fas fa-briefcase"></i>
                    <div class="content">
                        <span>09.2017 - 09.2018</span>
                        <h3>Junior Software Engineer (Tech Mahindra)</h3>
                        <p>Tech Mahindra is Vendor for IT Solution especially Dataware House in
                            H3I (Hutchison 3 Indonesi). I help for maintenance data and fix the issue
                            whether inside sql, report application, and the logic flow. Also
                            maintenance batch job for ETL process regarding report. The
                            technology is Oracle, posgre, greenplum, have a strong SQL or PL/SQL,
                            have good operations of linux, and have good knowlaged bash
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <!-------------------- Qualification Area (End) -------------------->



    <!-------------------- Services Area (Start) -------------------->
    <section class="service" id="service">

        <div class="heading">
            <h2>my <span>portfolio</span></h2>
        </div>

        <div class="box-container">

            <div class="service-item">
                <a href="{{ secure_url('https://www.g2academy.co/login#student') }}" target="_blank">
                    <i class="fas fa-chalkboard-teacher"></i>
                    <h3>LMS</h3>
                    <p>In G2Academy, the Learning Management System (LMS) enables</p>
                    <p>students and instructors to collaborate</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://admin.g2academy.co') }}" target="_blank">
                    <i class="fas fa-users-cog"></i>
                    <h3>Admin Learning Center</h3>
                    <p>In G2Academy, admins can easily manage materials, assignments,</p>
                    <p>and learning schedules for students</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://www.g2academy.co/programs') }}" target="_blank">
                    <i class="fas fa-search-dollar"></i>
                    <h3>Marketing Market Programs</h3>
                    <p>In G2Academy, marketing can easily show classes and</p>
                    <p>content for students</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://cms.g2academy.co/login') }}" target="_blank">
                    <i class="fas fa-newspaper"></i>
                    <h3>CMS</h3>
                    <p>In G2Academy, marketing can easily manage materials, classes</p>
                    <p>and content in website for students</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://crm.g2academy.co/login') }}" target="_blank">
                    <i class="fas fa-users"></i>
                    <h3>CRM</h3>
                    <p>The CRM at G2academy by marketing to manage student and corporate data</p>
                    <p>and recommend engaging programs</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://instructor.g2academy.co/login') }}" target="_blank">
                    <i class="fas fa-book-reader"></i>
                    <h3>Instructor Center</h3>
                    <p>In G2Academy, instructors can easily manage materials, assignments,</p>
                    <p>and learning for student</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://www.aeon.co.id') }}" target="_blank">
                    <i class="fas fa-globe"></i>
                    <h3>AEON Credit Product Services</h3>
                    <p>In AEON, marketing can easily show the products</p>
                    <p>and content for customers</p>
                </a>
            </div>

            <div class="service-item">
                <a href="{{ secure_url('https://secure.aeon.co.id/signin') }}" target="_blank">
                    <i class="fas fa-gifts"></i>
                    <h3>AEON Credit Customer Site</h3>
                    <p>In AEON, customers can easily manage their transaction,</p>
                    <p>balance, rewards and promo</p>
                </a>
            </div>

        </div>

    </section>
    <!-------------------- Services Area (End) -------------------->
@endsection
